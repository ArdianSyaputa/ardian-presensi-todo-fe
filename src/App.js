import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './Componen/Login';
import Register from './Componen/Register';
import Home from './Pages/Home';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/home" component={Home} exact />
            <Route path="/" component={Login} exact />
            <Route path="/register" component={Register} exact />
          </Switch>
        </main></BrowserRouter>
    </div>
  );
}

export default App;
