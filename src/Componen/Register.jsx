import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
export default function Register() {

  const history = useHistory();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const register = async (e) => {
    e.preventDefault();
    const fromData = {
    username: username,
    email: email,
    password: password,
    }
    try {
      const response = await axios.post(
        "http://localhost:2025/challange/register", fromData );
      if (response.status == 200) {
        Swal.fire({
          icon: 'success',
          title: 'Register Berhasil',
          showConfirmButton: false,
          timer: 1500
        })
        setTimeout(() => {
          history.push("/");
        }, 1250);
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <section class="h-screen">
    <div class="px-6 h-full text-gray-800">
  
      <div
        class="flex xl:justify-center lg:justify-between justify-center items-center flex-wrap h-full g-6"
      >
        <div
          class="grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 mb-12 md:mb-0"
        >
          <img
            src="https://png.pngtree.com/png-vector/20190119/ourlarge/pngtree-cartoon-hand-painted-keyboard-typing-hand-png-image_481349.jpg"
            class="w-full"
            alt="Sample image"
          />       
        </div>
        <div class="xl:ml-20 xl:w-5/12 lg:w-5/12 md:w-8/12 mb-12 md:mb-0">
        <h4 class="text-xl font-semibold mt-1 mb-12 pb-1 text-center">Silahkan Melakukan Register</h4>
          <form
            onSubmit={register}
            method="POST"
            action="">


          <div class="mb-6">
              <input
                type="text"
                class="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                id="exampleFormControlInput2"
                placeholder="Username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>

            <div class="mb-6">
              <input
                type="text"
                class="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                id="exampleFormControlInput2"
                placeholder="Email address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
  
           
            <div class="mb-6">
              <input
                type="password"
                class="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                id="exampleFormControlInput2"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
 
             
            <div class="text-center lg:text-left">
              <button
                type="button"
                class="inline-block px-7 py-3 bg-blue-600 text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                onClick={register}
              >
               Register
              </button>
              <p class="text-sm font-semibold mt-2 pt-1 mb-0">
                Sudah Mempunyai Akun?
                <a
                  href="/"
                  class="text-red-600 hover:text-red-700 focus:text-red-700 transition duration-200 ease-in-out"
                  >Login</a>
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  );
}
